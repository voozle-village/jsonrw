#ifndef _JSONWRITER_H_
#define _JSONWRITER_H_

#include <string>
#include <vector>

#include <cstdio>

namespace json
{
  class Writer
  {
  public:
    Writer(FILE* fp);
    Writer(const Writer&) = delete;
    Writer& operator=(const Writer&) = delete;
    
    void startObject();
    void startObject(const std::string& name);
    void endObject();

    void startArray();
    void startArray(const std::string& name);
    void endArray();

    void write(const std::string& value);
    void write(const std::string& name, const std::string& value);
    void write(const int value);
    void write(const std::string& name, const int value);
    void write(const float value);
    void write(const std::string& name, const float value);
    void write(const bool value);
    void write(const std::string& name, const bool value);

  private:
    enum State {
      Null,
      ArrayStart,
      Array,
      ObjectStart,
      Object,
      End,
    };

    State getState() const;
    void escape(const std::string& s);
    void writeName(const std::string& name);

    FILE* fp;
    std::vector<State> stack;
    std::string string;
  };
  
} // ns json

#endif /* _JSONWRITER_H_ */
