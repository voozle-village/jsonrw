#ifndef _JSONREADER_H_
#define _JSONREADER_H_

#include <functional>
#include <iostream>
#include <memory>
#include <string>

#include <cstdio>

namespace json
{
  class ArrayReader;
  class ObjectReader;

  class Reader
  {
  public:
    virtual ~Reader() {}
    virtual ArrayReader* onArray() = 0;
    virtual ObjectReader* onObject() = 0;
    virtual bool onValue(const std::string& value) = 0;

    virtual ArrayReader* onNamedArray(const std::string& name) = 0;
    virtual ObjectReader* onNamedObject(const std::string& name) = 0;
    virtual bool onNamedValue(const std::string& name, const std::string& value) = 0;

    virtual bool onComplete() = 0;
  };

  class ArrayReader : public Reader
  {
  public:
    ArrayReader* onNamedArray(const std::string& name) override;
    ObjectReader* onNamedObject(const std::string& name) override;
    bool onNamedValue(const std::string& name, const std::string& value) override;
  };

  class ObjectReader : public Reader
  {
  public:
    ArrayReader* onArray() override;
    ObjectReader* onObject() override;
    bool onValue(const std::string& value) override;
  };

  class SkippingArrayReader : public ArrayReader
  {
  public:
    ArrayReader* onArray() override;
    ObjectReader* onObject() override;
    bool onValue(const std::string& value) override;
    bool onComplete() override;
  };

  class SkippingObjectReader : public ObjectReader
  {
  public:
    ArrayReader* onNamedArray(const std::string& name) override;
    ObjectReader* onNamedObject(const std::string& name) override;
    bool onNamedValue(const std::string& name, const std::string& value) override;
    bool onComplete() override;
  };

  class ObjectArrayReader : public ArrayReader
  {
  public:
    typedef std::function<ObjectReader*(const size_t)> Constructor;
    typedef std::function<bool(const size_t)> Completer;

    ObjectArrayReader( Constructor constructor,
		       Completer completer );

    ArrayReader* onArray() override;
    ObjectReader* onObject() override;
    bool onValue(const std::string& value) override;
    bool onComplete() override;

  private:
    size_t index = 0;
    Constructor constructor;
    Completer completer;
  };

  template<class Converter, class Inserter, class Completer>
  class ValueArrayReader : public ArrayReader
  {
  public:
    typedef Converter ConverterType;
    typedef Inserter InserterType;
    typedef Completer CompleterType;

    ValueArrayReader(ConverterType converter, InserterType inserter, CompleterType completer)
      : converter(converter)
      , inserter(inserter)
      , completer(completer)
    {
    }

    bool onValue(const std::string& string) override
    {
      bool ok;
      auto value = converter(string, ok);
      if( !ok ) {
	std::cerr << "ERROR: Could not convert value '" << string << "'" << std::endl;
	return false;
      }
      else {
	*inserter++ = value;
	return true;
      }
    }

    ArrayReader* onArray() override
    {
      std::cerr << "ERROR: Unexpected array in array of values" << std::endl;
      return nullptr;
    }

    ObjectReader* onObject() override
    {
      std::cerr << "ERROR: Unexpected object in array of values" << std::endl;
      return nullptr;
    }

    bool onComplete() override
    {
      return completer();
    }

  private:
    ConverterType converter;
    InserterType inserter;
    CompleterType completer;
  };

  bool parse(FILE* fp, std::shared_ptr<Reader> reader);

} // ns json

#endif /* _JSONREADER_H_ */
