#include "jsonreader.h"

#include <iostream>
#include <memory>
#include <sstream>
#include <vector>

#include <cassert>
#include <cstdlib>
#include <cstring>

namespace json
{
  ArrayReader* ArrayReader::onNamedArray(const std::string& name)
  {
    std::cerr << "Unexpected named array '" << name << "'" << std::endl;
    return nullptr;
  }
  
  ObjectReader* ArrayReader::onNamedObject(const std::string& name)
  {
    std::cerr << "Unexpected named object '" << name << "'" << std::endl;
    return nullptr;
  }
  
  bool ArrayReader::onNamedValue(const std::string& name, const std::string& /*value*/)
  {
    std::cerr << "Unexpected named value '" << name << "'" << std::endl;
    return false;
  }

  ArrayReader* ObjectReader::onArray()
  {
    std::cerr << "Unexpected unnamed array" << std::endl;
    return nullptr;
  }
  
  ObjectReader* ObjectReader::onObject()
  {
    std::cerr << "Unexpected unnamed object" << std::endl;
    return nullptr;
  }
  
  bool ObjectReader::onValue(const std::string& /*value*/)
  {
    std::cerr << "Unexpected unnamed value" << std::endl;
    return false;
  }

  ArrayReader* SkippingArrayReader::onArray()
  {
    std::cerr << "Skipping array" << std::endl;
    return new SkippingArrayReader;
  }

  ObjectReader* SkippingArrayReader::onObject()
  {
    std::cerr << "Skipping object" << std::endl;
    return new SkippingObjectReader;
  }

  bool SkippingArrayReader::onValue(const std::string& /*value*/)
  {
    std::cerr << "Skipping value" << std::endl;
    return true;
  }

  bool SkippingArrayReader::onComplete()
  {
    return true;
  }

  ArrayReader* SkippingObjectReader::onNamedArray(const std::string& name)
  {
    std::cerr << "Skipping array '" << name << "'" << std::endl;
    return new SkippingArrayReader;
  }

  ObjectReader* SkippingObjectReader::onNamedObject(const std::string& name)
  {
    std::cerr << "Skipping object '" << name << "'" << std::endl;
    return new SkippingObjectReader;
  }

  bool SkippingObjectReader::onNamedValue(const std::string& name, const std::string& /*value*/)
  {
    std::cerr << "Skipping value '" << name << "'" << std::endl;
    return true;
  }

  bool SkippingObjectReader::onComplete()
  {
    return true;
  }

  ObjectArrayReader::ObjectArrayReader( Constructor constructor,
					Completer completer ) 
    : constructor(constructor)
    , completer(completer)
  {
  }
  
  ArrayReader* ObjectArrayReader::onArray()
  {
    std::cerr << "ERROR: Unexpected array in array of objects" << std::endl;
    return nullptr;
  }
  
  ObjectReader* ObjectArrayReader::onObject()
  {
    return constructor(index++);
  }
  
  bool ObjectArrayReader::onValue(const std::string& /*value*/)
  {
    std::cerr << "ERROR: Unexpected value in array of objects" << std::endl;
    return false;
  }
  
  bool ObjectArrayReader::onComplete()
  {
    return constructor(index);
  }

  namespace 
  {
    class Buffer
    {
    public:
      enum {
	ChunkSize = 1024 * 16,
	NumChunks = 5,
	Size = ChunkSize * NumChunks,
      };

      Buffer(FILE* fp)
        : fp(fp)
	, atEnd(feof(fp))
      {
	bytes[0] = '\0';
      }

      Buffer(const Buffer&) = delete;
      Buffer& operator=(const Buffer&) = delete;

      const char* getPtr()
      {
	return &bytes[start];
      }
  
      void advance(const size_t n)
      {
	assert(start + n <= end);
	start += n;
	pos += n;
      }

      size_t getBytesAvailable() const
      {
	return end - start;
      }

      bool prepare()
      {
	if( !atEnd && getBytesAvailable() <= ChunkSize ) {
	  fill();
	}
	return getBytesAvailable() > 0;
      }

      size_t getFilePos() const
      {
	return pos;
      }

    private:
      void fill()
      {
	// Preconditions:
	// - !atEnd
	// - getBytesAvailable() <= ChunkSize
	end = getBytesAvailable();
	if( start > 0 ) {
	  memmove(&bytes[0], &bytes[start], end);
	  start = 0;
	}
	const size_t numChars = ChunkSize * (NumChunks - 1);
	if( numChars > 0 ) {
	  end += fread(&bytes[end], sizeof(char), numChars, fp);
	  atEnd = feof(fp);
	}
	bytes[end] = '\0';
      }


      FILE* fp;
      size_t start = 0;
      size_t end = 0;
      size_t pos = 0;
      char bytes[Size + 1];
      bool atEnd;
    };

  } // ns

  static bool parseString(Buffer& buffer, std::string& s)
  {
    s.clear();
    bool escaped = false;
    while( buffer.prepare() ) {
      const char c = *buffer.getPtr();
      buffer.advance(1);
      if( escaped ) {
	escaped = false;
	switch(c) {
	case '"':
	case '\\':
	case '/':
	  s.push_back(c);
	  break;
	case 'b':
	  s.push_back('\b');
	  break;
	case 'f':
	  s.push_back('\f');
	  break;
	case 'n':
	  s.push_back('\n');
	  break;
	case 'r':
	  s.push_back('\r');
	  break;
	case 't':
	  s.push_back('\t');
	  break;
	default:
          return false;
	}
      }
      else if( c == '\\' ) {
	escaped = true;
      }
      else if( c == '"' ) {
        return true;
      }
      else {
	s.push_back(c);
      }
    }
    return false;
  }

  bool parse(FILE* fp, std::shared_ptr<Reader> reader)
  {
    std::vector<std::shared_ptr<Reader>> readers{ reader };
    std::string name;
    name.reserve(256);
    std::string string;
    string.reserve(256);

    enum {
      AcceptStartObject = 0x0001,
      AcceptEndObject   = 0x0002,
      AcceptStartArray  = 0x0004,
      AcceptEndArray    = 0x0008,
      AcceptComma       = 0x0010, 
      AcceptColon       = 0x0020, 
      AcceptValue       = 0x0040, 
      AcceptName        = 0x0080, 
      AcceptEnd         = 0x0100, 
      AcceptStart       = 0x0200, 
    };

    unsigned state = AcceptStart;

    auto setState = [&](const unsigned s) {
      #if 0
      std::cerr << "STATE:";
      if( s & AcceptStartObject ) std::cerr << " AcceptStartObject";
      if( s & AcceptEndObject ) std::cerr << " AcceptEndObject";
      if( s & AcceptStartArray ) std::cerr << " AcceptStartArray";
      if( s & AcceptEndArray ) std::cerr << " AcceptEndArray";
      if( s & AcceptComma ) std::cerr << " AcceptComma";
      if( s & AcceptColon ) std::cerr << " AcceptColon";
      if( s & AcceptValue ) std::cerr << " AcceptValue";
      if( s & AcceptName ) std::cerr << " AcceptName";
      if( s & AcceptEnd ) std::cerr << " AcceptEnd";
      if( s & AcceptStart ) std::cerr << " AcceptStart";
      std::cerr << std::endl;
      #endif
      state = s;
    };

    auto inArray = [&]() {
      return dynamic_cast<ArrayReader*>(readers.back().get()) != nullptr;
    };

    auto pushValue = [&](const std::string& value) {
      if( !(state & AcceptValue) ) {
	std::cerr << "ERROR: Unexpected value" << std::endl;
	return false;
      }
      if( !( name.empty()
	     ? readers.back()->onValue(value)
	     : readers.back()->onNamedValue(name, value) ) ) {
	std::cerr << "ERROR: Could not set value" << std::endl;
	return false;
      }
      name.clear();
      setState( AcceptComma | ( inArray()
				? AcceptEndArray
				: AcceptEndObject ) );
      return true;
    };

    auto pushArray = [&]() {
      if( !(state & AcceptStartArray) ) {
	std::cerr << "ERROR: Unexpected array" << std::endl;
	return false;
      }
      auto reader = std::shared_ptr<Reader>( name.empty()
					     ? static_cast<Reader*>( readers.back()->onArray() )
					     : static_cast<Reader*>( readers.back()->onNamedArray(name) ) );
      if( !reader ) {
	std::cerr << "ERROR: Could not add array" << std::endl;
	return false;
      }
      readers.push_back(reader);
      name.clear();
      setState( AcceptValue | AcceptStartArray | AcceptStartObject | AcceptEndArray);
      return true;
    };

    auto pushObject = [&]() {
      if( !(state & AcceptStartObject) ) {
	std::cerr << "ERROR: Unexpected object" << std::endl;
	return false;
      }
      auto reader = std::shared_ptr<Reader>( name.empty()
					     ? static_cast<Reader*>( readers.back()->onObject() )
					     : static_cast<Reader*>( readers.back()->onNamedObject(name) ) );
      if( !reader ) {
	std::cerr << "ERROR: Could not add object" << std::endl;
	return false;
      }
      readers.push_back(reader);
      name.clear();
      setState( AcceptName | AcceptEndObject );
      return true;
    };

    Buffer buffer(fp);

    while( buffer.prepare() ) {
      const size_t filePos = buffer.getFilePos();
      const char c = *buffer.getPtr();
      if( isspace(c) ) {
        buffer.advance(1);
        continue;
      }

      char* end;
      strtof(buffer.getPtr(), &end);
      if( end != buffer.getPtr() ) {
        string.assign(buffer.getPtr(), end - buffer.getPtr());
        buffer.advance(string.size());
        if( !pushValue(string) ) {
	  std::cerr << "ERROR: Error at position 0x" << std::hex << filePos << std::endl;
          return false;
        }
        continue;
      }
      else {
        buffer.advance(1);
      }

      switch( c ) {

      case '"':
        {
          if( !(state & (AcceptName | AcceptValue)) ) {
            std::cerr << "ERROR: Unexpected string" << std::endl;
	    std::cerr << "ERROR: Error at position 0x" << std::hex << filePos << std::endl;
            return false;
          }
          if( !parseString(buffer, string) ) {
	    std::cerr << "ERROR: Invalid string" << std::endl;
	    std::cerr << "ERROR: Error at position 0x" << std::hex << filePos << std::endl;
	    return false;
	  }
	  if( state & AcceptName ) {
	    if( string.empty() ) {
	      std::cerr << "ERROR: Empty name" << std::endl;
	      std::cerr << "ERROR: Error at position 0x" << std::hex << filePos << std::endl;
	      return false;
	    }
	    name = string;
	    setState( AcceptColon );
	  }
	  else if( !pushValue(string) ) {
	    return false;
	  }
	}
        break;

      case ':':
        if( !(state & AcceptColon ) ) {
          std::cerr << "ERROR: Unexpected colon" << std::endl;
	  std::cerr << "ERROR: Error at position 0x" << std::hex << filePos << std::endl;
          return false;
        }
        setState( AcceptValue | AcceptStartArray | AcceptStartObject );
        break;

      case '{':
        if( state & AcceptStart ) {
          setState( AcceptName | AcceptEndObject );
        }
        else {
          if( !pushObject() ) {
	    std::cerr << "ERROR: Error at position 0x" << std::hex << filePos << std::endl;
            return false;
          }
	}
        break;

      case '}':
        if( !(state & AcceptEndObject) ) {
          std::cerr << "ERROR: Unexpected end of object" << std::endl;
	  std::cerr << "ERROR: Error at position 0x" << std::hex << filePos << std::endl;
          return false;
        }
        if( !readers.back()->onComplete() ) {
          std::cerr << "ERROR: Object was incomplete" << std::endl;
	  std::cerr << "ERROR: Error at position 0x" << std::hex << filePos << std::endl;
          return false;
        }
        readers.pop_back();
        if( readers.empty() ) {
          setState( AcceptEnd );
        }
        else {
          setState( AcceptComma | ( inArray()
                                    ? AcceptEndArray
                                    : AcceptEndObject ) );
        }
        break;

      case '[':
        if( !pushArray() ) {
	  std::cerr << "ERROR: Error at position 0x" << std::hex << filePos << std::endl;
          return false;
        }
        break;

      case ']':
        if( !(state & AcceptEndArray) ) {
          std::cerr << "ERROR: Unexpected end of array" << std::endl;
	  std::cerr << "ERROR: Error at position 0x" << std::hex << filePos << std::endl;
          return false;
        }
        if( !readers.back()->onComplete() ) {
          std::cerr << "ERROR: Array was incomplete" << std::endl;
	  std::cerr << "ERROR: Error at position 0x" << std::hex << filePos << std::endl;
          return false;
        }
        readers.pop_back();
        if( readers.empty() ) {
          std::cerr << "ERROR: Stack underflow" << std::endl;
	  std::cerr << "ERROR: Error at position 0x" << std::hex << filePos << std::endl;
          return false;
        }
        else {
          setState( AcceptComma | ( inArray()
                                    ? AcceptEndArray
                                    : AcceptEndObject ) );
        }
        break;

      case ',':
        if( !(state & AcceptComma ) ) {
          std::cerr << "ERROR: Unexpected comma" << std::endl;
	  std::cerr << "ERROR: Error at position 0x" << std::hex << filePos << std::endl;
          return false;
        }
        if( inArray() ) {
          setState( AcceptValue | AcceptStartArray | AcceptStartObject | AcceptEndArray );
        }
        else {
          setState( AcceptName | AcceptEndObject );
        }
        break;

      default: 
        std::cerr << "ERROR: Unexpected input '" << c << "' at position 0x" << std::hex << filePos << std::endl;
        return false;
      }
    }
  
    return state == AcceptEnd;
  }

} // ns json
