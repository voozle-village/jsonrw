#include "jsonwriter.h"

#include <cassert>

namespace json
{
  Writer::Writer(FILE* fp)
    : fp(fp)
  {
    string.reserve(256);
  }
    
  void Writer::startObject()
  {
    const State state = getState();
    assert( state == Null || state == ArrayStart || state == Array );
    if( state == Array ) {
      fprintf(fp, ",{");
    }
    else {
      fprintf(fp, "{");
      if( state != Null ) {
	stack.back() = Array;
      }
    }
    stack.push_back(ObjectStart);
  }

  void Writer::startObject(const std::string& name)
  {
    writeName(name);
    fprintf(fp, "{");
    stack.push_back(ObjectStart);
  }

  void Writer::endObject()
  {
    const State state = getState();
    assert( state == ObjectStart || state == Object );
    fprintf(fp, "}");
    stack.pop_back();
    if( stack.empty() ) {
      stack.push_back(End);
    }
  }

  void Writer::startArray()
  {
    const State state = getState();
    assert( state == ArrayStart || state == Array );
    if( state == Array ) {
      fprintf(fp, ",[");
    }
    else {
      fprintf(fp, "[");
      stack.back() = Array;
    }
    stack.push_back(ArrayStart);
  }

  void Writer::startArray(const std::string& name)
  {
    writeName(name);
    fprintf(fp, "[");
    stack.push_back(ArrayStart);
  }

  void Writer::endArray()
  {
    const State state = getState();
    assert( state == ArrayStart || state == Array );
    fprintf(fp, "]");
    stack.pop_back();
  }

  void Writer::write(const std::string& value)
  {
    const State state = getState();
    assert( state == ArrayStart || state == Array );
    escape(value);
    if( state == Array ) {
      fprintf(fp, ",\"%s\"", string.c_str());
    }
    else {
      fprintf(fp, "\"%s\"", string.c_str());
      stack.back() = Array;
    }
  }

  void Writer::write(const std::string& name, const std::string& value)
  {
    writeName(name);
    escape(value);
    fprintf(fp, "\"%s\"", string.c_str());
  }

  void Writer::write(const int value)
  {
    const State state = getState();
    assert( state == ArrayStart || state == Array );
    if( state == Array ) {
      fprintf(fp, ",%d", value);
    }
    else {
      fprintf(fp, "%d", value);
      stack.back() = Array;
    }
  }

  void Writer::write(const std::string& name, const int value)
  {
    writeName(name);
    fprintf(fp, "%d", value);
  }

  void Writer::write(const float value)
  {
    const State state = getState();
    assert( state == ArrayStart || state == Array );
    if( state == Array ) {
      fprintf(fp, ",%f", static_cast<double>(value));
    }
    else {
      fprintf(fp, "%f", static_cast<double>(value));
      stack.back() = Array;
    }
  }

  void Writer::write(const std::string& name, const float value)
  {
    writeName(name);
    fprintf(fp, "%f", static_cast<double>(value));
  }

  void Writer::write(const bool value)
  {
    const State state = getState();
    assert( state == ArrayStart || state == Array );
    if( state == Array ) {
      fprintf(fp, ",\"%s\"", value ? "true" : "false");
    }
    else {
      fprintf(fp, "\"%s\"", value ? "true" : "false");
      stack.back() = Array;
    }
  }

  void Writer::write(const std::string& name, const bool value)
  {
    writeName(name);
    fprintf(fp, "\"%s\"", value ? "true" : "false");
  }

  Writer::State Writer::getState() const
  {
    return stack.empty()
      ? Null
      : stack.back();
  }

  void Writer::escape(const std::string& s)
  {
    string.clear();
    for( size_t i = 0; i < s.size(); ++i ) {
      const char c = s[i];
      switch(c) {
      case '"':
      case '\\':
      case '/':
	string.push_back('\\');
	string.push_back(c);
	break;
      case '\b':
	string.push_back('\\');
	string.push_back('b');
	break;
      case '\f':
	string.push_back('\\');
	string.push_back('f');
	break;
      case '\n':
	string.push_back('\\');
	string.push_back('n');
	break;
      case '\r':
	string.push_back('\\');
	string.push_back('r');
	break;
      case '\t':
	string.push_back('\\');
	string.push_back('t');
	break;
      default:
	string.push_back(c);
      }
    }
  }

  void Writer::writeName(const std::string& name)
  {
    const State state = getState();
    assert( state == ObjectStart || state == Object );
    escape(name);
    if( state == Object ) {
      fprintf(fp, ",\"%s\":", string.c_str());
    }
    else {
      fprintf(fp, "\"%s\":", string.c_str());
      stack.back() = Object;
    }
  }
  
} // ns json
