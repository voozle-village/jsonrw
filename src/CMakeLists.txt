project(jsonrw)
cmake_minimum_required(VERSION 2.8)

set(jsonrw_sources jsonreader.cpp jsonwriter.cpp)
add_library(jsonrw ${jsonrw_sources})
